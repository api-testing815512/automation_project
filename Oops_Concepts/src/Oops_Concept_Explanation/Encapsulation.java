package Oops_Concept_Explanation;

public class Encapsulation {

        String name ;
		int age;
		
		public void ReadInfo() {
			System.out.println("My name is " +name+" And Age :" +age);
		}
		
		public void DisplayInfo() {
			System.out.println("Name Display is " +name+"  And His Age is " +age);
		}
		
		public static void main(String[] args) {
			
			Encapsulation abc = new Encapsulation ();
			abc. name = "Ana";
		    abc.age =26;
		    
		    abc.ReadInfo();
		    abc.DisplayInfo();
		    
		

	}
}

