package Oops_Concept_Explanation;

public class ReplaceNewInfo extends ReplaceInfo {
	
	
	public void Read() {
		System.out.println("Firstly read new Information");
	}
	
	public void Write() {
		System.out.println("Write new Information");
	}

	public static void main(String[] args) {
		ReplaceInfo replace1 = new ReplaceNewInfo();
		replace1.Read();
		replace1.Write();
		replace1.Replace();

	}

}
