package Oops_Concept_Explanation;

public abstract class ReplaceInfo {
		
		//abstract method
		public abstract void Read();
		
		public abstract void Write();

		//non abstract method
		public void Replace() {
			System.out.println("Follow instruction to replace Info");

		}

	}

