package Oops_Concept_Explanation;

public class Class_Explanation {

	String name;
	int age;
	
	public void ReadInfo() {
		System.out.println("My name is: " +name+" And Age :" +age);
	}
	
	public void DisplayInfo() {
		System.out.println("Name Display is  " +name+"  And His Age is " +age);
	}

	public static void main(String[] args) {
		
		Class_Explanation Person1 = new Class_Explanation();
	    Person1.name="Alex";
	    Person1.age=24;
        Person1.ReadInfo();
        Person1.DisplayInfo();
	}

}