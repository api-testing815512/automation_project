Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header is :
Date=Wed, 08 May 2024 07:19:42 GMT
Content-Type=application/json; charset=utf-8
Content-Length=84
Connection=keep-alive
Report-To={"group":"heroku-nel","max_age":3600,"endpoints":[{"url":"https://nel.heroku.com/reports?ts=1715152781&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=VDO8goX%2FfhFe5QiltWJCJAqw2MRNRCHponHRO1NeaY8%3D"}]}
Reporting-Endpoints=heroku-nel=https://nel.heroku.com/reports?ts=1715152781&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=VDO8goX%2FfhFe5QiltWJCJAqw2MRNRCHponHRO1NeaY8%3D
Nel={"report_to":"heroku-nel","max_age":3600,"success_fraction":0.005,"failure_fraction":0.05,"response_headers":["Via"]}
X-Powered-By=Express
Access-Control-Allow-Origin=*
Etag=W/"54-X98zorFcdIUbHjFIcpJ47eDglY8"
Via=1.1 vegur
CF-Cache-Status=DYNAMIC
Server=cloudflare
CF-RAY=8807aed5aa0f59b2-DEL

Response body is :
{"name":"morpheus","job":"leader","id":"389","createdAt":"2024-05-08T07:19:41.936Z"}