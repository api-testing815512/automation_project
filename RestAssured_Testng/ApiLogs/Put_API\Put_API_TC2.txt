Endpoint is :
https://reqres.in/api/users/2

Request body is :
{
    "name": "morpheus",
    "job": "zion resident"
}

Response header is :
Date=Wed, 08 May 2024 07:19:45 GMT
Content-Type=application/json; charset=utf-8
Transfer-Encoding=chunked
Connection=keep-alive
Report-To={"group":"heroku-nel","max_age":3600,"endpoints":[{"url":"https://nel.heroku.com/reports?ts=1715152784&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=hR5UnpRCvMGlwqmav8UjrWehR6we2uUGN8%2FtttmaR18%3D"}]}
Reporting-Endpoints=heroku-nel=https://nel.heroku.com/reports?ts=1715152784&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=hR5UnpRCvMGlwqmav8UjrWehR6we2uUGN8%2FtttmaR18%3D
Nel={"report_to":"heroku-nel","max_age":3600,"success_fraction":0.005,"failure_fraction":0.05,"response_headers":["Via"]}
X-Powered-By=Express
Access-Control-Allow-Origin=*
Etag=W/"50-ojsaEUgih+iQsB0Kbep11WXuGSY"
Via=1.1 vegur
CF-Cache-Status=DYNAMIC
Vary=Accept-Encoding
Server=cloudflare
CF-RAY=8807aee8b85e59bd-DEL
Content-Encoding=gzip

Response body is :
{"name":"morpheus","job":"zion resident","updatedAt":"2024-05-08T07:19:44.984Z"}