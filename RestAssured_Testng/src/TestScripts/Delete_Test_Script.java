package TestScripts;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import CommonMethods.API_Trigger;
import CommonMethods.Utilities;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class Delete_Test_Script extends API_Trigger {
	File logfolder;
	Response response;
	ResponseBody responseBody;

	@BeforeTest
	public void setup() {
		logfolder = Utilities.createFolder("delete_API");
	}

	@Test
	public void validate() {

		response = delete_API_Trigger(delete_request_body(), delete_endpoint());
		int statuscode = response.statusCode();
		responseBody = response.getBody();

		Assert.assertEquals(statuscode, 204, "Correct status code not found even after retrying for 5 times");

	}

	@AfterTest
	public void teardown() throws IOException {
		Utilities.createLogFile("delete_API_TC1", logfolder, delete_endpoint(), delete_request_body(),
				response.getHeaders().toString(), responseBody.asString());
	}
}
