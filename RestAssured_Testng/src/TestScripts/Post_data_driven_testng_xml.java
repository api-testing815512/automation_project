package TestScripts;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import CommonMethods.API_Trigger;
import CommonMethods.Testng_Retry_analyser;
import CommonMethods.Utilities;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Post_data_driven_testng_xml extends API_Trigger {

	File logfolder;
	Response response;
	ResponseBody responseBody;

	@BeforeTest
	public void setup() {
		logfolder = Utilities.createFolder("Post_API");
	}
     
	@Parameters({"req_name","req_job"})
	@Test (retryAnalyzer = Testng_Retry_analyser.class , description = "Validate the responseBody parameters of Post_TC_1")
	public void validate( String req_name, String req_job) {
		String requestBody = "{\r\n" + "    \"name\": \"" + req_name +" \",\r\n" + "    \"job\": \"" + req_job +"\"\r\n" + "}";
		
		response = Post_API_Trigger(requestBody, post_endpoint());

		int statuscode = response.statusCode();
		responseBody = response.getBody();

		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_id = responseBody.jsonPath().getString("id");
		String res_createdAt = responseBody.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.toString().substring(0, 11);

		JsonPath jsp_req = new JsonPath(post_request_body());
		String Req_name = jsp_req.getString("name");
		String Req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		Assert.assertEquals(statuscode, 201, "Correct status code not found even after retrying for 5 times");
		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");
		Assert.assertNotNull(res_id, "Id in ResponseBody is found to be null");
		Assert.assertEquals(res_createdAt, expecteddate, "createdAt in ResponseBody is not equal to Date Generated");

	}

	@AfterTest
	public void teardown() throws IOException {
		Utilities.createLogFile("Post_API_TC1", logfolder, post_endpoint(), post_request_body(),
				response.getHeaders().toString(), responseBody.asString());

	}

}