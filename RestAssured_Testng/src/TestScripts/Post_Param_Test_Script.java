package TestScripts;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import CommonMethods.API_Trigger;
import CommonMethods.Utilities;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.response.ResponseOptions;

public class Post_Param_Test_Script extends API_Trigger {

	public static void execute() throws IOException {

		File logfolder = Utilities.createFolder("Post_API");

		Response res = Post_API_Trigger(post_param_requestBody("TC1"), post_endpoint());

		int statuscode = res.statusCode();
		System.out.println(statuscode);

		for (int i = 0; i < 5; i++) {

			if (statuscode == 201) {
				ResponseBody responseBody = res.getBody();
				System.out.println(responseBody.asString());
				Utilities.createLogFile("Post_API_TC1", logfolder, post_endpoint(), post_request_body(),
						res.getHeaders().toString(), responseBody.asString());
				validate(responseBody);
				break;
			} else {
				System.out.println("Status code found in iteration :" + i + " is :" + statuscode
						+ ", and is not equal expected status code hence retrying");
			}
		}

		Assert.assertEquals(statuscode, 201, "Correct status code not found even after retrying for 5 times");

	}

	public static void validate(ResponseBody responseBody) throws IOException {

		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_id = responseBody.jsonPath().getString("id");
		String res_createdAt = responseBody.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.toString().substring(0, 11);

		JsonPath jsp_req = new JsonPath(post_param_requestBody("TC1"));
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");
		Assert.assertNotNull(res_id, "Id in ResponseBody is found to be null");
		Assert.assertEquals(res_createdAt, expecteddate, "createdAt in ResponseBody is not equal to Date Generated");

	}
}