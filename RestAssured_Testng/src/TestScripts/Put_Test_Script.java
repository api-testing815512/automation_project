package TestScripts;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import CommonMethods.API_Trigger;
import CommonMethods.Testng_Retry_analyser;
import CommonMethods.Utilities;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Put_Test_Script extends API_Trigger {
	File logfolder;
	Response response;
	ResponseBody responseBody;

	@DataProvider()
	public Object[][] requestBody() {
		return new Object[][] {

				{ "morpheus", "leader" }, { "Rutuja", "Tester" } };
	}

	@BeforeTest
	public void setup() {

		logfolder = Utilities.createFolder("Put_API");
	}

	@Test(retryAnalyzer = Testng_Retry_analyser.class, dataProvider = "requestBody", description = "validat the parameters of put_api_TC_1")
	public void validate_Put(String req_name, String req_job) {
		String requestBody = "{\r\n" + "    \"name\": \"" + req_name + "\",\r\n" + "    \"job\": \"" + req_job
				+ "\"\r\n" + "}";

		response = put_API_Trigger(put_request_body(), put_endpoint());
		int statuscode = response.statusCode();
		responseBody = response.getBody();
		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_updatedAt = responseBody.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.toString().substring(0, 11);

		JsonPath jsp_req = new JsonPath(put_request_body());
		String req_name1 = jsp_req.getString("name");
		String req_job1 = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		Assert.assertEquals(res_name, req_name1, "Name in ResponseBody is not equal to Name sent in Request Body");
		Assert.assertEquals(res_job, req_job1, "Job in ResponseBody is not equal to Job sent in Request Body");

		Assert.assertEquals(res_updatedAt, expecteddate, "createdAt in ResponseBody is not equal to Date Generated");

	}

	@AfterTest
	public void teardown() throws IOException {
		Utilities.createLogFile("Put_API_TC2", logfolder, put_endpoint(), put_request_body(),
				response.getHeaders().toString(), responseBody.asString());
	}
}