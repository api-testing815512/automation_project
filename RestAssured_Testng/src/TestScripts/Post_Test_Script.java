package TestScripts;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import CommonMethods.API_Trigger;
import CommonMethods.Testng_Retry_analyser;
import CommonMethods.Utilities;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Post_Test_Script extends API_Trigger {

	File logfolder;
	Response response;
	ResponseBody responseBody;

	@BeforeTest
	public void setup() {
		logfolder = Utilities.createFolder("Post_API");
	}

	@Test (retryAnalyzer = Testng_Retry_analyser.class , description = "Validate the responseBody parameters of Post_TC_1")
	public void validate() {

		response = Post_API_Trigger(post_request_body(), post_endpoint());

		int statuscode = response.statusCode();
		System.out.println(statuscode);
		
		 responseBody = response.getBody();

		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_id = responseBody.jsonPath().getString("id");
		String res_createdAt = responseBody.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.toString().substring(0, 11);

		JsonPath jsp_req = new JsonPath(post_request_body());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		Assert.assertEquals(statuscode, 201, "Correct status code not found even after retrying for 5 times");
		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");
		Assert.assertNotNull(res_id, "Id in ResponseBody is found to be null");
		Assert.assertEquals(res_createdAt, expecteddate, "createdAt in ResponseBody is not equal to Date Generated");

	}

	@AfterTest
	public void teardown() throws IOException {
		Utilities.createLogFile("Post_API_TC1", logfolder, post_endpoint(), post_request_body(),
				response.getHeaders().toString(), responseBody.asString());
	}
		
		public void validate1() {
			
			response = Post_API_Trigger(post_request_body(),post_endpoint());
			
			responseBody = response.getBody();
			
			int statuscode = response.getStatusCode();
			System.out.println(statuscode);
			
			String res_name = responseBody.jsonPath().getString("name");
			String res_job = responseBody.jsonPath().getString("job");
			String res_id = responseBody.jsonPath().getString("res_job");
			String res_createdAt = responseBody.jsonPath().getString("created_At");
			res_createdAt = res_createdAt.toString().substring(0,11);
			
			JsonPath jsp_req = new JsonPath(post_request_body());
			
			String req_name = jsp_req.getString("name");
			String req_job = jsp_req.getString("job");
			String req_id = jsp_req.getString("id");
			
			LocalDateTime currentdate = LocalDateTime.now();
			String expectedDate = res_createdAt.toString().substring(0,11);
			
			Assert.assertEquals(statuscode, 201,"status code is not found even after retrying 5 times");
			Assert.assertEquals(res_name, req_name,"Name in responsebody is not eequal to name in request body");
			Assert.assertEquals(res_job, req_job,"job in reesponsebody is not equal to job in requeestbody");
			Assert.assertEquals(res_id, req_id,"id in responsebody is not equal to id in requestbody");
			Assert.assertEquals(res_createdAt, expectedDate,"createdAt is not equal to expecteddate");
			
			
			
	}

}