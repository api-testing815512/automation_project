package TestScripts;


import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;



import CommonMethods.API_Trigger;
import CommonMethods.Testng_Retry_analyser;
import CommonMethods.Utilities;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Post_dataprovider_differentclass extends API_Trigger {

	File logfolder;
	Response response;
	ResponseBody responseBody;

	@BeforeTest
	public void Setup() {
		logfolder = Utilities.createFolder("post_API");

	}

	@Test(retryAnalyzer = Testng_Retry_analyser.class, dataProvider = "requestBody", dataProviderClass = EnvironmentandRepository.testng_dataprovider.class, description = "validat the parameters of post_api_TC_1")
	public void validate_Post(String req_name, String req_job) {
		String requestBody = "{\r\n" + "    \"name\": \"" + req_name + "\",\r\n" + "    \"job\": \"" + req_job
				+ "\"\r\n" + "}";
		response = Post_API_Trigger(requestBody, post_endpoint());
		int statuscode = response.statusCode();
		responseBody = response.getBody();
		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_id = responseBody.jsonPath().getString("id");
		String res_createdAt = responseBody.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.toString().substring(0, 11);

		// 3 : fetch the requestBody parameter

		JsonPath jsp_req = new JsonPath(post_request_body());

		String Req_name = jsp_req.getString("name");
		String Req_job = jsp_req.getString("job");

		// 5 : Generate expected Date

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		// step 6: validation using TestNG Assertion
		Assert.assertEquals(statuscode, 201, "correct statuscode  and even after retrying for 5 time");
		Assert.assertEquals(res_name, req_name, " Name in ResponseBody is not equal to name sent in RequestBody");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to job sent in RequestBody");
		Assert.assertNotNull(res_id, "id in ResponseBody is found to be null");
		Assert.assertEquals(res_createdAt, expecteddate, "createdAt in ResponseBody is not equal to Date generated");

	}

	@AfterTest
	public void teardown() throws IOException {
		Utilities.createLogFile("post_API", logfolder, post_endpoint(), post_request_body(),
				response.getHeaders().toString(), responseBody.asString());
	}

}