package Common_Utilities;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentListenerClass implements ITestListener{
	
	ExtentSparkReporter sparkreporter;
	ExtentReports extentReport;
	ExtentTest test;
	
	public void reportConfigurations() {
		sparkreporter = new ExtentSparkReporter("./extent-report/report.html");
		extentReport = new ExtentReports();
		
		extentReport.attachReporter(sparkreporter);
		
		//adding system/env info to reports
		extentReport.setSystemInfo("OS", "mac os x");
		extentReport.setSystemInfo("user", "Users");
		
		//configurations for changing look and feel of report
		sparkreporter.config().setDocumentTitle("RestAssured Extent Listener Report");
		sparkreporter.config().setReportName("This is my first Extent-Report");
		sparkreporter.config().setTheme(Theme.DARK);
	}
	
	public void onStart(ITestContext result) {
		reportConfigurations();
		System.out.println("On start method invoked...");
	}
	
	public void onFinish(ITestContext result) {
		System.out.println("On finish method invoked...");
		extentReport.flush();
	}
	
	public void onTestFailure(ITestResult result) {
		System.out.println("Name of test method failed: " + result.getName());
		test = extentReport.createTest(result.getName());
		test.log(Status.FAIL, MarkupHelper.createLabel("Name of the failed test case is: " + result.getName(), ExtentColor.RED));
	}
	
	public void onTestSkipped(ITestResult result) {
		System.out.println("Name of test method skipped: " + result.getName());
		test = extentReport.createTest(result.getName());
		test.log(Status.SKIP, MarkupHelper.createLabel("Name of the skipped test case is: " + result.getName(), ExtentColor.YELLOW));
	}
	
	public void onTestStart(ITestResult result) {
		System.out.println("Name of test method started: " + result.getName());
	}
	
	public void onTestSuccess(ITestResult result) {
		System.out.println("Name of test method executed successfully: " + result.getName());
		test = extentReport.createTest(result.getName());
		test.log(Status.PASS, MarkupHelper.createLabel("Name of the successfully executed test case is: " + result.getName(), ExtentColor.GREEN));
	}
	
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
	}

}
