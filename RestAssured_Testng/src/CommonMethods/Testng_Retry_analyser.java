package CommonMethods;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class Testng_Retry_analyser implements IRetryAnalyzer {

	private int start = 0;
	private int end = 5;

	public boolean retry(ITestResult result) {

		if (start < end) {
			String testcasname = result.getName();
			System.out.println(
					testcasname + "Failed in current Iteration " + start + ", Hence retrying for " + (start + 1));
			start++;
			return true;
		}
		return false;
	}

}