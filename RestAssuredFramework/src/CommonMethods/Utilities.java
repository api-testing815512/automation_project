package CommonMethods;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Utilities {

	public static File createFolder(String foldername) {

		// Step 1 : Fetch the current Java project

		String projectFolder = System.getProperty("user.dir");
		System.out.println(projectFolder);

		// Step 2 : Check if foldername coming in variable foldername already exists in
		// projectFolder and create foldername accordingly

		File folder = new File(projectFolder + "//ApiLogs//" + foldername);

		if (folder.exists()) {
			System.out.println(folder + " , Already exists in Java Project :" + projectFolder);
		} else {
			System.out.println(folder + " , Doesn't exists in Java Project :" + projectFolder + ", " + "hence creating it");
			folder.mkdir();
			System.out.println(folder + " , Created in Java Project :" + projectFolder);
		}

		return folder;

	}

	public static void createLogFile(String Filename, File logfolder, String endpoint, String requestBody,
			String responseHeader, String responseBody) throws IOException {

		// Step 1 : Create and open a text file
		File newTextFile = new File(logfolder + "\\" + Filename + ".txt");
		System.out.println("File create with name :" + newTextFile.getName());

		// Step 2 : Write data into the file
		FileWriter writedata = new FileWriter(newTextFile);
		writedata.write("Endpoint is :\n" + endpoint + "\n\n");
		writedata.write("Request body is :\n" + requestBody + "\n\n");
		writedata.write("Response header is :\n" + responseHeader + "\n\n");
		writedata.write("Response body is :\n" + responseBody);

		// Step 3 : Save and close the file
		writedata.close();

	}

	public static ArrayList<String> ReadExcelData(String sheetname, String testcase) throws IOException {

		ArrayList<String> arrayData = new ArrayList<String>();

		// get the location of java project
		String project_dir = System.getProperty("user.dir");
		
		//  Read the file using file input stream
		FileInputStream fis = new FileInputStream(project_dir + "//DataFiles//InputData.xlsx");
		
		// create object and open the file using XSSF workbook
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		
		//Count the no of sheets in the file
		int countofsheets = wb.getNumberOfSheets();
		// System.out.println(wb.getNumberOfSheets());
		
		//Read the names of sheets from excel file
		for (int i = 0; i < countofsheets; i++) {
			
			//Check the desired sheet name
			if (wb.getSheetName(i).equals(sheetname)) {
				// System.out.println(wb.getSheetName(i));
				
				
				XSSFSheet sheet = wb.getSheetAt(i);
				Iterator<Row> rows = sheet.iterator();
				while (rows.hasNext()) {
					Row datarows = rows.next();
					String testcaseName = datarows.getCell(0).getStringCellValue();
					// System.out.println(testcaseName);
					if (testcaseName.equals(testcase)) {
						Iterator<Cell> cellvalues = datarows.iterator();
						while (cellvalues.hasNext()) {
							String testdata = "";
							Cell cell = cellvalues.next();
							CellType datatype = cell.getCellType();
							if (datatype.toString().equals("STRING")) {
								testdata = cell.getStringCellValue();
							} else if (datatype.toString().equals("NUMERIC")) {
								Double num_testdata = cell.getNumericCellValue();
								testdata = String.valueOf(num_testdata);

							}
							System.out.println(testdata);
							arrayData.add(testdata);
						}
						break;
					}
				}
				break;
			} else {
				System.out.println("No sheet found of name : " + sheetname + " in current iteration : " + i);
			}
		}
		wb.close();
		return arrayData;
	}
}
