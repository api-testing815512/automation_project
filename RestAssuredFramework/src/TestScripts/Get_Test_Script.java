package TestScripts;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

import org.testng.Assert;

import CommonMethods.API_Trigger;
import CommonMethods.Utilities;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class Get_Test_Script extends API_Trigger {

	public static void execute() throws IOException {
		File logfolder = Utilities.createFolder("get_API");
		int statuscode = 0;
		for (int i = 0; i < 5; i++) {
			Response response = get_API_Trigger(get_request_body(), get_endpoint());
			statuscode = response.statusCode();
			System.out.println(statuscode);
			if (statuscode == 200) {
				ResponseBody responseBody = response.getBody();
				System.out.println(responseBody.asString());
				Utilities.createLogFile("get_API_TC1", logfolder, get_endpoint(), get_request_body(),
						response.getHeaders().toString(), responseBody.asString());
				validate(responseBody);
				break;
			} else {
				System.out.println("Status code found in iteration :" + i + " is :" + statuscode
						+ ", and is not equal expected status code hence retrying");
			}
		}

		Assert.assertEquals(statuscode, 200, "Correct status code not found even after retrying for 5 times");

	}

	public static void validate(ResponseBody responseBody) {

		int exp_page = 2;
		int exp_per_page = 6;
		int exp_total = 12;
		int exp_total_pages = 2;

		// Step 1.2 : Declare expected results of data array
		int[] exp_id = { 7, 8, 9, 10, 11, 12 };
		String[] exp_email = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		String[] first_name = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String[] last_name = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
		String[] avatar = { "https://reqres.in/img/faces/7-image.jpg", "https://reqres.in/img/faces/8-image.jpg",
				"https://reqres.in/img/faces/9-image.jpg", "https://reqres.in/img/faces/10-image.jpg",
				"https://reqres.in/img/faces/11-image.jpg", "https://reqres.in/img/faces/12-image.jpg" };

		// Step 1.3 : Declare expected results of support

		String exp_url = "https://reqres.in/#support-heading";
		String exp_text = "To keep ReqRes free, contributions towards server costs are appreciated!";

		// Step 2 : Declare the needed variables
		String hostname = "https://reqres.in";
		String resource = "/api/users?page=2";

		// Step 3: Trigger the API

		// Step 4.1 : Build the request specification using RequestSpecification
		RequestSpecification req_spec = RestAssured.given();

		// Step 4.2 : Trigger the API
		Response response = req_spec.get(hostname + resource);

		// Step 5: Extract the status code
		int statuscode = response.statusCode();
		System.out.println(statuscode);

		// Step 6: Fetch the response body parameters

		// Step 6.1 : Fetch page parameters

		int res_page = responseBody.jsonPath().getInt("page");
		int res_per_page = responseBody.jsonPath().getInt("per_page");
		int res_total = responseBody.jsonPath().getInt("total");
		int res_total_pages = responseBody.jsonPath().getInt("total_pages");

		// Step 6.1: Fetch Size of data array
		List<String> dataArray = responseBody.jsonPath().getList("data");
		int sizeofarray = dataArray.size();

		// Step 7 : Validation

		// Step 7.1 : Validate per page parameters
		Assert.assertEquals(res_page, exp_page);
		Assert.assertEquals(res_per_page, exp_per_page);
		Assert.assertEquals(res_total, exp_total);
		Assert.assertEquals(res_total_pages, exp_total_pages);

		// Step 7.2 : Validate data array
		for (int i = 0; i < sizeofarray; i++) {
			Assert.assertEquals(Integer.parseInt(responseBody.jsonPath().getString("data[" + i + "].id")), exp_id[i]);
			Assert.assertEquals(responseBody.jsonPath().getString("data[" + i + "].email"), exp_email[i]);
			Assert.assertEquals(responseBody.jsonPath().getString("data[" + i + "].first_name"), first_name[i]);

			Assert.assertEquals(responseBody.jsonPath().getString("data[" + i + "].last_name"), last_name[i]);

			Assert.assertEquals(responseBody.jsonPath().getString("data[" + i + "].avatar"), avatar[i]);

		}

		// Step 7.3 : Validate support json
		Assert.assertEquals(responseBody.jsonPath().getString("support.url"), exp_url);
		Assert.assertEquals(responseBody.jsonPath().getString("support.text"), exp_text);

	}
}