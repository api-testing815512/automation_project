package TestScripts;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;

import CommonMethods.API_Trigger;
import CommonMethods.Utilities;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Patch_Test_Script extends API_Trigger {

	public static void execute() throws IOException {

		File logfolder = Utilities.createFolder("Patch_API");

		int statuscode = 0;
		for (int i = 0; i < 5; i++) {
			Response response = patch_API_Trigger(patch_request_body(), patch_endpoint());

			 statuscode = response.statusCode();
			System.out.println(statuscode);

			if (statuscode == 200) {
				ResponseBody responseBody = response.getBody();
				System.out.println(responseBody.asString());
				Utilities.createLogFile("Patch_API_TC1", logfolder, patch_endpoint(), patch_request_body(),
						response.getHeaders().toString(), responseBody.asString());
				validate(responseBody);
				break;
			} else {
				System.out.println("Status code found in iteration :" + i + " is :" + statuscode
						+ ", and is not equal expected status code hence retrying");
			}
		}

		Assert.assertEquals(statuscode, 200, "Correct status code not found even after retrying for 5 times");

	}

	public static void validate(ResponseBody responseBody) {

		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_updatedAt = responseBody.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.toString().substring(0, 11);

		JsonPath jsp_req = new JsonPath(patch_request_body());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");
		
		Assert.assertEquals(res_updatedAt, expecteddate, "updatedAt in ResponseBody is not equal to Date Generated");

	
	}
}