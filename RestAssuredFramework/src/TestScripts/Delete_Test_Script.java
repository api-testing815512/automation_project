package TestScripts;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

import org.testng.Assert;

import CommonMethods.API_Trigger;
import CommonMethods.Utilities;
import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class Delete_Test_Script extends API_Trigger {

	public static void execute() throws IOException {
		File logfolder = Utilities.createFolder("delete_API");
		int statuscode = 0;
		for (int i = 0; i < 5; i++) {
			Response response = delete_API_Trigger(delete_request_body(), delete_endpoint());
			statuscode = response.statusCode();
			System.out.println(statuscode);
			if (statuscode == 204) {
				ResponseBody responseBody = response.getBody();
				System.out.println(responseBody.asString());
				Utilities.createLogFile("delete_API_TC1", logfolder, delete_endpoint(), delete_request_body(),
						response.getHeaders().toString(), responseBody.asString());

				break;
			} else {
				System.out.println("Status code found in iteration :" + i + " is :" + statuscode
						+ ", and is not equal expected status code hence retrying");
			}
		}

		Assert.assertEquals(statuscode, 204, "Correct status code not found even after retrying for 5 times");

	}
}
