Endpoint is :
https://reqres.in/api/users/2

Request body is :
{
    "name": "morpheus",
    "job": "zion resident"
}

Response header is :
Date=Tue, 07 May 2024 14:48:48 GMT
Content-Type=application/json; charset=utf-8
Transfer-Encoding=chunked
Connection=keep-alive
Report-To={"group":"heroku-nel","max_age":3600,"endpoints":[{"url":"https://nel.heroku.com/reports?ts=1715093327&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=LL0LMj0GLmaZrItYw%2F5vd2ssHryJu5DK0Kix2lCWrOk%3D"}]}
Reporting-Endpoints=heroku-nel=https://nel.heroku.com/reports?ts=1715093327&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=LL0LMj0GLmaZrItYw%2F5vd2ssHryJu5DK0Kix2lCWrOk%3D
Nel={"report_to":"heroku-nel","max_age":3600,"success_fraction":0.005,"failure_fraction":0.05,"response_headers":["Via"]}
X-Powered-By=Express
Access-Control-Allow-Origin=*
Etag=W/"50-u28g3BHrxP7QIZuplLClHwPrx1w"
Via=1.1 vegur
CF-Cache-Status=DYNAMIC
Vary=Accept-Encoding
Server=cloudflare
CF-RAY=880203519bba9fad-SIN
Content-Encoding=gzip

Response body is :
{"name":"morpheus","job":"zion resident","updatedAt":"2024-05-07T14:48:47.991Z"}