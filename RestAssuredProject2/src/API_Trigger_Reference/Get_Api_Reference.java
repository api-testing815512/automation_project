package API_Trigger_Reference;

import java.util.List;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
import io.restassured.path.json.JsonPath;

public class Get_Api_Reference {
	
	
	public static void main(String[] args) {
		
		int exp_page=2;
		int exp_per_page=6;
		int exp_total=12;
		int exp_total_pages=2;

		
		int[] exp_id= {7,8,9,10,11,12};
		String[] exp_email = {"michael.lawson@reqres.in", "lindsay.ferguson@reqres.in",
				"tobias.funke@reqres.in","byron.fields@reqres.in",
				"george.edwards@reqres.in","rachel.howell@reqres.in"};
		String[]  exp_first_name= {"Michael","Lindsay","Tobias","Byron","George","Rachel"};
		String[] exp_last_name= {"Lawson","Ferguson","Funke","Fields","Edwards","Howell"};
		String[] exp_avatar= {"https://reqres.in/img/faces/7-image.jpg",
				"https://reqres.in/img/faces/8-image.jpg",
				"https://reqres.in/img/faces/9-image.jpg",
				"https://reqres.in/img/faces/10-image.jpg",
				"https://reqres.in/img/faces/11-image.jpg",
				"https://reqres.in/img/faces/12-image.jpg"};
		
		
	
		String exp_url = "https://reqres.in/#support-heading";
		String exp_text ="To keep ReqRes free, contributions towards server costs are appreciated!";
		
		String hostname = "https://reqres.in";
		String resource = "/api/users?page=2";
		

		RequestSpecification req_spec = RestAssured.given();
		
		Response response=req_spec.get(hostname+resource);
		
		int statuscode=response.statusCode();
		System.out.println(statuscode);
		
		ResponseBody responseBody = response.getBody();
		System.out.println(responseBody.asString());
		
		
		int res_page=responseBody.jsonPath().getInt("page");
		int res_per_page=responseBody.jsonPath().getInt("per_page");
		int res_total=responseBody.jsonPath().getInt("total");
		int res_total_pages=responseBody.jsonPath().getInt("total_pages");
		
		List<String>dataArray=responseBody.jsonPath().getList("data");
		int sizeofarray = dataArray.size();
		
		Assert.assertEquals(res_page,exp_page);
		Assert.assertEquals(res_per_page,exp_per_page);
		Assert.assertEquals(res_total,exp_total);
		Assert.assertEquals(res_total_pages,exp_total_pages);
		
		for(int i=0; i<sizeofarray; i++) {
		
		Assert.assertEquals(Integer.parseInt(responseBody.jsonPath().getString("data["+i+"].id")),exp_id[i],"Validation of id failed for json object at indeex:" +i);
		Assert.assertEquals(responseBody.jsonPath().getString("data["+i+"].email"),exp_email[i],"Validation of email failed for json object at index:" +i);
		Assert.assertEquals(responseBody.jsonPath().getString("data["+i+"].first_name"),exp_first_name[i],"Validation of first name failed for json object at index:" +i);
		Assert.assertEquals(responseBody.jsonPath().getString("data["+i+"].last_name"),exp_last_name[i],"Validation of last name failed for json object at index:" +i);
		Assert.assertEquals(responseBody.jsonPath().getString("data["+i+"].avatar"),exp_avatar[i],"Validation of avatar failed for json object at index:" +i);
		
	} 
	
	Assert.assertEquals(responseBody.jsonPath().getString("support.url"),exp_url,"Validation of URL failed");
	Assert.assertEquals(responseBody.jsonPath().getString("support.text"),exp_text,"Validation of text failed");
	
	}

}
