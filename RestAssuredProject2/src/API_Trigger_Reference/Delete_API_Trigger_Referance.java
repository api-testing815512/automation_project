package API_Trigger_Reference;

import static io.restassured.RestAssured.given;

import io.restassured.RestAssured;

public class Delete_API_Trigger_Referance {

	public static void main(String[] args) {
		
		String hostname = "https://reqres.in/";
		String resource = "/api/users/2";
		

		 RestAssured.baseURI= hostname;


     String responseBody = given().when().delete(resource).then().extract().response().asString();
		 
		 System.out.println(responseBody);



	}

}
