package API_Trigger_Reference;

import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class Rest_Post_API_Reference_RequestSpecification_Response_Class {
	
	public static void main(String[] args) {
		
		
		
		//Step 1: Declare the needed variables
		
		
		String hostname = "https://reqres.in";
		String resource = "/api/users";
		String headername = "Content-Type";
		String headervalue = "application/json";
		String requestBody = "{\n"
				+ "    \"name\": \"morpheus\",\n"
				+ "    \"job\": \"leader\"\n"
				+ "}";
		
		//Step 2:Trigger the API
		
		//Step 2.1: Build the request specification using RequestSpecification
		
		RequestSpecification req_spec = RestAssured.given();
		
		//Steep 2.2:Set the header
		
		req_spec.header(headername,headervalue);
		
		//Step 2.3:Set the request body
		
		req_spec.body(requestBody);
		
		//Step 2.4: Trigger the API
		
		Response response=req_spec.post(hostname+resource);
		
		//Step 3:Extract the status code
		
		int statuscode=response.statusCode();
		System.out.println(statuscode);
		 
		//Step 4:Fetch the response body parameters
		ResponseBody responseBody = response.getBody();
		System.out.println(responseBody.asString());
		
		 String res_name = responseBody.jsonPath().getString("name");
		 String res_job = responseBody.jsonPath().getString("job");
		 String res_id =  responseBody.jsonPath().getString("id");
		 String res_createdAt = responseBody.jsonPath().getString("createdAt");
		 res_createdAt = res_createdAt.toString().substring(0,11);
		 
		 
		 //Step 5: Fetch the request body parameters
		 
		 JsonPath jsp_req = new JsonPath(requestBody);
		 String req_name = jsp_req.getString("name");
		 String req_job = jsp_req.getString("job");
		 
		 //Step 6:Generate expected data
		 
		 LocalDateTime currentdate = LocalDateTime.now();
		 String expecteddate=currentdate.toString().substring(0, 11);
		 
		 //step 6.3:Use TestNG's Assert
		 
		 Assert.assertEquals(res_name,req_name,"Name in ResponseBody is not equal to Name sent in RequestBody");
		 Assert.assertEquals(res_job,req_job,"Job in ResponseBody is not equal to Job sent in RequestBody");
		 Assert.assertNotNull(res_id,"Id in ResponseBody is found to be null");
		 Assert.assertEquals(res_createdAt,expecteddate,"createdAt in ResponseBody is not equal to Date generated"); 
		 
		  
		 
		 
		 
		
		
		
	}

}
