package API_Trigger_Reference;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;
import org.testng.Assert;

public class Post_API_Trigger_Referance {
	
	
	public static void main(String[] args) {
		 
		
		//Step 1: Declare the needed variables
		
		
		String hostname = "https://reqres.in/";
		String resource = "/api/users";
		String headername = "Content-Type";
		String headervalue = "application/json";
		String requestBody = "{\n"
				+ "    \"name\": \"morpheus\",\n"
				+ "    \"job\": \"leader\"\n"
				+ "}";
		
		 //step2: Declare the baseURI
		 RestAssured.baseURI= hostname;
		 
		//Step 3: Configure the API and trigger
		 
			// given().header(headername,headervalue).body(requestBody).log().all().when().post(resource).then().extract().response().asString();
			
		 

		 //step 4: Configure the API for execution and Save the response in a string variable 
		
		 String responseBody = given().header(headername,headervalue).body(requestBody).when().post(resource).then().extract().response().asString();
		 
		 System.out.println(responseBody);
		 
		 
		 //step 5: Parse the response body 
		 //step 5.1: Create the object of json path 
		 JsonPath jsp_res = new JsonPath(responseBody);
		 
		 //Step 5.2:Parse the individual params using jsp_res object
		 
		 String res_name = jsp_res.getString("name");
		 System.out.println(res_name);
		 String res_job = jsp_res.getString("job");
		 System.out.println(res_job);
		 String res_id = jsp_res.getString("id");
		 System.out.println(res_id);
		 String res_createdAt = jsp_res.getString("createdAt");
		 res_createdAt = res_createdAt.substring(0,11);
		 System.out.println(res_createdAt);
		 
		 //step  6:Validate the response body
		 
		 //step 6.1: parse request body and save into local variables
		 
		 JsonPath jsp_req = new JsonPath(requestBody);
		 String req_name = jsp_req.getString("name");
		 String req_job = jsp_req.getString("job");
		 
		 //Step 6.2: Generate expected date
		 
		 LocalDateTime currentdate = LocalDateTime.now();
		 String expecteddate=currentdate.toString().substring(0, 11);
		 
		 //step 6.3:Use TestNG's Assert
		 
		 Assert.assertEquals(res_name,req_name);
		 Assert.assertEquals(res_job,req_job);
		 Assert.assertNotNull(res_id);
		 Assert.assertEquals(res_createdAt,expecteddate); 
		 
		 
	}
     
	 
	  
	  
}
