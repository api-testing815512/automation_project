package API_Trigger_Reference;


import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class Patch_API_Trigger_Referance {

	public static void main(String[] args) {

		String hostname = "https://reqres.in/";
		String resource = "/api/users/2";
		String headername = "Content-Type";
		String headervalue = "application/json";
		String requestBody = "{\n"
				+ "    \"name\": \"morpheus\",\n"
				+ "    \"job\": \"zion resident\"\n"
				+ "}";
	
		 RestAssured.baseURI= hostname;


 String responseBody = given().header(headername,headervalue).body(requestBody).when().patch(resource).then().extract().response().asString();
		 
		 System.out.println(responseBody);
		 

		 
		 JsonPath jsp_res = new JsonPath(responseBody);
		 
		 String res_name = jsp_res.getString("name");
		 System.out.println(res_name);
		 String res_job = jsp_res.getString("job");
		 System.out.println(res_job);
		
		 String res_updatedAt = jsp_res.getString("updatedAt");
		 res_updatedAt = res_updatedAt.substring(0,11);
		 System.out.println(res_updatedAt);
		 
		 JsonPath jsp_req = new JsonPath(requestBody);
		 String req_name = jsp_req.getString("name");
		 String req_job = jsp_req.getString("job");
		 
		 
		 LocalDateTime currentdate = LocalDateTime.now();
		 String expecteddate=currentdate.toString().substring(0, 11);
		 
		 
		 Assert.assertEquals(res_name,req_name);
		 Assert.assertEquals(res_job,req_job);
		 Assert.assertEquals(res_updatedAt,expecteddate); 

	}

}
