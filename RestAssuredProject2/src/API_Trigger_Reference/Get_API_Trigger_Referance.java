package API_Trigger_Reference;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class Get_API_Trigger_Referance {

	public static void main(String[] args) {
	
		String hostname = "https://reqres.in/";
		String resource = "/api/users?page=2";

		
		int id[]= {7,8,9,10,11,12};
		String email[] = {"michael.lawson@reqres.in", "lindsay.ferguson@reqres.in",
				"tobias.funke@reqres.in","byron.fields@reqres.in",
				"george.edwards@reqres.in","rachel.howell@reqres.in"};
		String first_name[]= {"Michael","Lindsay","Tobias","Byron","George","Rachel"};
		String last_name[]= {"Lawson","Ferguson","Funke","Fields","Edwards","Howell"};
		String avatar[]= {"https://reqres.in/img/faces/7-image.jpg",
				"https://reqres.in/img/faces/8-image.jpg",
				"https://reqres.in/img/faces/9-image.jpg",
				"https://reqres.in/img/faces/10-image.jpg",
				"https://reqres.in/img/faces/11-image.jpg",
				"https://reqres.in/img/faces/12-image.jpg"};
		
		
		
		 RestAssured.baseURI= hostname;
    String responseBody = given().when().get(resource).then().extract().response().asString();
		 
		 System.out.println(responseBody);
		 
		 JsonPath jsp_res= new JsonPath(responseBody);
		 
		 
		 int data = jsp_res.getInt("data.size()");
		 System.out.println(data);
		 
		 for(int i=0; i<data; i++)
		 {
			 int exp_id = id[i];
			 String exp_email= email[i];
			 String exp_first_name= first_name[i];
			 String exp_last_name= last_name[i];
			 String exp_avatar= avatar[i];
			 
			 int res_id=jsp_res.getInt("data ["+i+"].id");
			 String res_email = jsp_res.getString("data ["+i+"].email");
			 String res_first_name = jsp_res.getString("data ["+i+"].first_name");
			 String res_last_name = jsp_res.getString("data ["+i+"].last_name");
			 String res_avatar = jsp_res.getString("data ["+i+"].avatar");
			 
			 
			 Assert.assertEquals(res_id, exp_id);
			 
			 Assert.assertEquals(res_email, exp_email);
			 
			 Assert.assertEquals(res_first_name, exp_first_name);
			 Assert.assertEquals(res_last_name, exp_last_name);
			 Assert.assertEquals(res_avatar, exp_avatar);
			 
		 }
	

	}

}
