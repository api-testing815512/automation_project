package API_Trigger_Reference;

import java.time.LocalDateTime;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class Rest_Put_API_Reference_RequestSpeecification_Response_Class {

	public static void main(String[] args) {

		String hostname = "https://reqres.in";
		String resource =  "/api/users/2";
		String headername = "Content-Type";
		String headervalue = "application/json";
		String requestBody = "{\n"
				+ "    \"name\": \"morpheus\",\n"
				+ "    \"job\": \"leader\"\n"
				+ "}";
	RequestSpecification req_spec = RestAssured.given();
		
		
		
		req_spec.header(headername,headervalue);
		
		
		req_spec.body(requestBody);
		
		
		
		Response response=req_spec.put(hostname+resource);
		

		
		int statuscode=response.statusCode();
		System.out.println(statuscode);
		 
		
		ResponseBody responseBody = response.getBody();
		System.out.println(responseBody.asString());
		
		 String res_name = responseBody.jsonPath().getString("name");
		 String res_job = responseBody.jsonPath().getString("job");
		 String res_updatedAt = responseBody.jsonPath().getString("updatedAt");
		 res_updatedAt = res_updatedAt.toString().substring(0,11);
		 
		 
		
		 
		 JsonPath jsp_req = new JsonPath(requestBody);
		 String req_name = jsp_req.getString("name");
		 String req_job = jsp_req.getString("job");
		 
	
		 
		 LocalDateTime currentdate = LocalDateTime.now();
		 String expecteddate=currentdate.toString().substring(0, 11);
		 
		 
		 Assert.assertEquals(res_name,req_name,"Name in ResponseBody is not equal to Name sent in RequestBody");
		 Assert.assertEquals(res_job,req_job,"Job in ResponseBody is not equal to Job sent in RequestBody");
		 Assert.assertEquals(res_updatedAt,expecteddate,"createdAt in ResponseBody is not equal to Date generated"); 
		 
		  
	}

}
