@Get_Api
Feature: Retrieve list of users from GET

  Scenario: Get users from page 2 
    Given the API endpoint is to get user details
    When I send a GET request to the endpoint
    Then the response status should be 200
    And the response body should contain a list of users