@Patch_Api
Feature: Trigger the PATCH API with required parameters

Scenario: Trigger the API request with valid request body parameters
    Given Update NAME and JOB in patch request body
    When Send the PATCH request with payload 
    Then Validate the status code for patch
    And Validate the patch response body parameters
    
    @Patch_Api   
Scenario Outline: Test Patch api with multiple data set 
    Given Enter "<NAME>" and "<JOB>" in request body
    When Send the request with payload
    Then Validate status code 
    And Validate response body parameters

Examples:
    |NAME|JOB|
    |ALEX|LEEADER|
    |KIYAN|MANAGER|
    |ANA|BA|