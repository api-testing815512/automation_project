@Put_Api
Feature: Trigger the PUT API with required parameters

Scenario: Trigger the API request with valid request body parameters
    Given Update NAME and JOB in request body
    When Send the PUT request with payload 
    Then Validate the status code 
    And Validate the response body parameters
    
@Put_Api   
Scenario Outline: Test PUT api with multiple data set 
    Given Enter "<NAME>" and "<JOB>" in request body
    When Send the request with payload
    Then Validate status code 
    And Validate response body parameters

Examples:
    |NAME|JOB|
    |ALEX|LEEADER|
    |KIYAN|MANAGER|
    |ANA|BA|