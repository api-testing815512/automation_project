Feature: Trigger the POST API with required parameters

Scenario: Trigger the API request with valid request body parameters
    Given Enter NAME and JOB in request body
    When Send the request with payload
    Then Validate status code 
    And Validate response body parameters
    
@Post_Api   
Scenario Outline: Test post api with multiple data set 
    Given Enter "<NAME>" and "<JOB>" in request body
    When Send the request with payload
    Then Validate status code 
    And Validate response body parameters

Examples:
    |NAME|JOB|
    |RUTUJA|TESTER|
    |SONAM|DEV|
    |AARTI|PHARMACIST|