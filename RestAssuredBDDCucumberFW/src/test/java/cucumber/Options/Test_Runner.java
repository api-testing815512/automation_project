package cucumber.Options;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
@RunWith(Cucumber.class)
@CucumberOptions(features="src/test/java/Features",glue= {"stepDefinitions"},tags="@Post_Api or @Put_Api or @Patch_Api or @Get_Api or @Delete_Api")

public class Test_Runner {

}


