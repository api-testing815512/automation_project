package stepDefinitions;

import java.time.LocalDateTime;

import org.testng.Assert;

import CommonMethods.API_Trigger;
import EnvironmentandRepository.Environment;
import EnvironmentandRepository.RequestRepository;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Put_API_StepDef {

	String requestbody;
	String endpoint;
	Response response;
	ResponseBody responseBody;
	

@Given("Update NAME and JOB in request body")
public void update_name_and_job_in_request_body() {
	requestbody = RequestRepository.put_request_body();
	endpoint = Environment.put_endpoint();
    // Write code here that turns the phrase above into concrete actions
   // throw new io.cucumber.java.PendingException();
}

@When("Send the PUT request with payload")
	public void send_the_put_request_with_payload() {
		response = API_Trigger.put_API_Trigger(requestbody, endpoint);
 //throw new io.cucumber.java.PendingException();
	}



@Then("Validate the status code")
public void validate_the_status_code() {
	int statuscode = response.statusCode();
	Assert.assertEquals(statuscode, 200, "Correct status code not found even after retrying for 5 times");
    // Write code here that turns the phrase above into concrete actions
   // throw new io.cucumber.java.PendingException();
}

@Then("Validate the response body parameters")
public void validate_the_response_body_parameters() {
    // Write code here that turns the phrase above into concrete actions
	responseBody = response.getBody();
	String res_name = responseBody.jsonPath().getString("name");
	String res_job = responseBody.jsonPath().getString("job");
	String res_updatedtedAt = responseBody.jsonPath().getString("updatedAt");
	res_updatedtedAt = res_updatedtedAt.toString().substring(0, 11);

	JsonPath jsp_req = new JsonPath(requestbody);
	String req_name = jsp_req.getString("name");
	String req_job = jsp_req.getString("job");

	LocalDateTime currentdate = LocalDateTime.now();
	String expecteddate = currentdate.toString().substring(0, 11);

	Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
	Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");
	Assert.assertEquals(res_updatedtedAt, expecteddate, "createdAt in ResponseBody is not equal to Date Generated");

    //throw new io.cucumber.java.PendingException();
}



}
