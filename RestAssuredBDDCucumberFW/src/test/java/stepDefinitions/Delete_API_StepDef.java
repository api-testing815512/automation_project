package stepDefinitions;

import org.testng.Assert;

import CommonMethods.API_Trigger;
import EnvironmentandRepository.Environment;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Delete_API_StepDef {
	String endpoint;
	Response response;
	ResponseBody responseBody;

	@Given("Enter endpoint for deleteing user")
	public void enter_endpoint_for_deleteing_user() {

		endpoint = Environment.delete_endpoint();
//	    throw new io.cucumber.java.PendingException();
	}
	
	@When("Send the dalete request with payload")
	public void send_the_dalete_request_with_payload() {
		response = API_Trigger.delete_API_Trigger(endpoint, endpoint);
	    // Write code here that turns the phrase above into concrete actions
	   // throw new io.cucumber.java.PendingException();
	}
	@Then("Validate delete response status code")
	public void validate_delete_response_status_code() {
		int statuscode = response.statusCode();
		Assert.assertEquals(statuscode, 204, "correct statuscode  and even after retrying for 5 time");
	    //throw new io.cucumber.java.PendingException();
	}
	@Then("The responsebody should be empty")
	public void the_responsebody_should_be_empty() {
    //throw new io.cucumber.java.PendingException();
	}

}
