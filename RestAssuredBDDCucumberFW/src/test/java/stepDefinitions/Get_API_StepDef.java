package stepDefinitions;

import java.util.List;

import org.testng.Assert;

import CommonMethods.API_Trigger;
import EnvironmentandRepository.Environment;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;


public class Get_API_StepDef {
	
	
	String endpoint;
	Response response;
	ResponseBody responseBody;
	@Given("the API endpoint is to get user details")
	public void the_api_endpoint_is_to_get_user_details() {
		endpoint = Environment.get_endpoint();
	}
	@When("I send a GET request to the endpoint")
	public void i_send_a_get_request_to_the_endpoint() {
		response = API_Trigger.get_API_Trigger(endpoint, endpoint);
	}
	@Then("the response status should be {int}")
	public void the_response_status_should_be(Integer int1) {
		int statuscode = response.statusCode();
		Assert.assertEquals(statuscode, 200, "correct statuscode  and even after retrying for 5 time");	  
	}
	@Then("the response body should contain a list of users")
	public void the_response_body_should_contain_a_list_of_users() {
		
		responseBody = response.getBody();

		int exp_page = 2;
		int exp_per_page = 6;
		int exp_total = 12;
		int exp_total_pages = 2;

		// Step 1.2 : Declare expected results of data array
		int[] exp_id = { 7, 8, 9, 10, 11, 12 };
		String[] exp_email = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		String[] first_name = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String[] last_name = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
		String[] avatar = { "https://reqres.in/img/faces/7-image.jpg", "https://reqres.in/img/faces/8-image.jpg",
				"https://reqres.in/img/faces/9-image.jpg", "https://reqres.in/img/faces/10-image.jpg",
				"https://reqres.in/img/faces/11-image.jpg", "https://reqres.in/img/faces/12-image.jpg" };

		// Step 1.3 : Declare expected results of support

		String exp_url = "https://reqres.in/#support-heading";
		String exp_text = "To keep ReqRes free, contributions towards server costs are appreciated!";

		

		int res_page = responseBody.jsonPath().getInt("page");
		int res_per_page = responseBody.jsonPath().getInt("per_page");
		int res_total = responseBody.jsonPath().getInt("total");
		int res_total_pages = responseBody.jsonPath().getInt("total_pages");
		
		
		List<String> dataArray = responseBody.jsonPath().getList("data");
		int sizeofarray = dataArray.size();
		
		
		Assert.assertEquals(res_page, exp_page);
		Assert.assertEquals(res_per_page, exp_per_page);
		Assert.assertEquals(res_total, exp_total);
		Assert.assertEquals(res_total_pages, exp_total_pages);
		
		for (int i = 0; i < sizeofarray; i++) {
			Assert.assertEquals(Integer.parseInt(responseBody.jsonPath().getString("data[" + i + "].id")), exp_id[i],
					"Validation of id failed for json object at index : " + i);
			Assert.assertEquals(responseBody.jsonPath().getString("data[" + i + "].email"), exp_email[i],
					"Validation of email failed for json object at index : " + i);
			Assert.assertEquals(responseBody.jsonPath().getString("data[" + i + "].first_name"), first_name[i],
					"validation of first_name failed for json object at index : " + i);
			Assert.assertEquals(responseBody.jsonPath().getString("data[" + i + "].last_name"), last_name[i],
					"validation of last_name failed for json object at index : " + i);
			Assert.assertEquals(responseBody.jsonPath().getString("data[" + i + "].avatar"), avatar[i],
					"validation avatar failed for json object at index : " + i);
	   // throw new io.cucumber.java.PendingException();
		
	}
		
		

	    
	}

}